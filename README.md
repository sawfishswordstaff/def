# def

minimal en_us dictionary cli app written in python

uses [free dictionary api](https://dictionaryapi.dev/)

# contents

1. [def](#def)
2. [contents](#contents)
3. [install](#install)
	1. [non windows](#non-windows)
	2. [windows](#windows)
4. [help](#help)

# install
## non windows
requirements
- python3
- python3-pip
- git

With python 3 installed, simply run `make setup install` with root privileges

- `setup` installs dependencies found in [requirements.txt](./requirements.txt)
- `install` copies [`def`](./def) into `/usr/local/bin/def`

## windows
requirements
- use git bash, wsl or something

repeat steps above but replace python3 with python 

# help
there is no help command so here's how to use it

```bash
# /path/to/repo/
# this assumes that def is somewhere in your PATH (environment variables)
def WORD
```
replace `WORD` with a word you want defined. that is all.

# licensing
0bsd

# see also
## mentions
- [free dictionary api by meetdeveloper](https://dictionaryapi.dev)
